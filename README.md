# Theme Library

## Pages

- [AdBlock Detection](https://monstm.gitlab.io/theme-library/html/adblock-detection.html)
- [Data Clipboard](https://monstm.gitlab.io/theme-library/html/data-clipboard.html)
- [Data Link](https://monstm.gitlab.io/theme-library/html/data-link.html)
- [Data Search](https://monstm.gitlab.io/theme-library/html/data-search.html)
- [Data Toggle](https://monstm.gitlab.io/theme-library/html/data-toggle.html)
- [URL Params](https://monstm.gitlab.io/theme-library/html/url-params.html?title=param+title&content=param+content&value=param+value)
- [UTM Builder](https://monstm.gitlab.io/theme-library/html/utm-builder.html)

## How To Use

```js
import adblockDetection from 'theme-library/module/adblock-detection'
import dataClipboard from 'theme-library/module/data-clipboard'
import dataLink from 'theme-library/module/data-link'
import dataSearch from 'theme-library/module/data-search'
import dataToggle from 'theme-library/module/data-toggle'
import urlParams from './module/url-params'
import utmBuilder from './module/utm-builder'

const head = document.querySelector('head')
const body = document.querySelector('body')
window.addEventListener('load', () => {
  adblockDetection(head, body)
  urlParams()
  utmBuilder()
})
body.addEventListener('click', (event) => {
  dataClipboard(event)
  dataLink(event)
  dataToggle(event)
})
body.addEventListener('submit', dataSearch)
```

-- OR --

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    ...
    <script
      defer
      src="https://cdn.jsdelivr.net/npm/theme-library/js/bundle.js"
    ></script>
    ...
  </head>
  <body>
    ...
  </body>
</html>
```
