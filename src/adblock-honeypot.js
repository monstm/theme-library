const currentScript = document.currentScript
const dataKey = currentScript.hasAttribute('data-key') ? currentScript.getAttribute('data-key') : null
const dataValue = currentScript.hasAttribute('data-value') ? currentScript.getAttribute('data-value') : null

if (dataKey && dataValue) {
  window[dataKey] = dataValue
}
