import adblockDetection from './module/adblock-detection'
import dataClipboard from './module/data-clipboard'
import dataLink from './module/data-link'
import dataSearch from './module/data-search'
import dataToggle from './module/data-toggle'
import urlParams from './module/url-params'
import utmBuilder from './module/utm-builder'

const head = document.querySelector('head')
const body = document.querySelector('body')

window.addEventListener('load', () => {
  adblockDetection(head, body)
  urlParams()
  utmBuilder()
})

body.addEventListener('click', (event) => {
  dataClipboard(event)
  dataLink(event)
  dataToggle(event)
})

body.addEventListener('submit', dataSearch)
