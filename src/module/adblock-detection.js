/**
 * Checks if adblock is detected and if so, clear the HTML head and body and
 * display a message asking the user to disable their adblock.
 *
 * @param {HTMLHeadElement} headElement - The HTML head element to clear if adblock is detected.
 * @param {HTMLBodyElement} bodyElement - The HTML body element to clear if adblock is detected.
 * @param {string} [version='latest'] - The version of the theme library to use. Defaults to 'latest'.
 * @return {undefined} This function does not return a value.
 */
const adblockDetection = (headElement, bodyElement, version = 'latest') => {
  const generateRandomString = () => Math.random().toString(36).substring(2)
  const dataKey = '_' + generateRandomString()
  const dataValue = generateRandomString()

  const checkAdblock = () => {
    if (window[dataKey] !== dataValue) {
      if (headElement instanceof HTMLHeadElement) {
        headElement.innerHTML = ''
      }

      if (bodyElement instanceof HTMLBodyElement) {
        bodyElement.innerHTML =
          '<h1>Adblock Detected</h1><p>Please disable your adblock</p>'
      }
    }
  }

  const scriptElement = document.createElement('script')
  scriptElement.type = 'text/javascript'
  scriptElement.src = `https://cdn.jsdelivr.net/npm/theme-library@${version}/js/adhelper.js`
  scriptElement.dataset.key = dataKey
  scriptElement.dataset.value = dataValue
  scriptElement.addEventListener('load', checkAdblock)
  scriptElement.addEventListener('error', checkAdblock)

  headElement.appendChild(scriptElement)
}

export default adblockDetection
