/**
 * Copies the value of the data-clipboard attribute of the closest element that
 * matches that selector to the clipboard.
 *
 * @param {Event} event - The event object that triggered the function.
 * @return {undefined} This function does not return a value.
 */
const dataClipboard = (event) => {
  if (!navigator?.clipboard?.writeText) return

  const target = event.target.closest('[data-clipboard]')
  if (!target) return

  const data = target.getAttribute('data-clipboard')
  navigator.clipboard.writeText(data)
}

export default dataClipboard
