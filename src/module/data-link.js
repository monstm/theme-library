/**
 * Redirects to the URL specified in the 'data-link' attribute of the given event target.
 * @param {Event} event - The event object that triggered the function.
 * @return {undefined} This function does not return a value.
 */
const dataLink = (event) => {
  const target = event.target.closest('[data-link]')
  if (!target) return

  const redirect = target.getAttribute('data-link')
  setTimeout(() => {
    window.location.href = redirect
  }, 100)
}

export default dataLink
