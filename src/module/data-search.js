/**
 * Searches for data based on the given event.
 *
 * @param {Event} event - The event object triggered by the search.
 * @return {boolean} Returns true if the search is successful; otherwise, false.
 */
const dataSearch = (event) => {
  const form = event.target
  if (!(form instanceof HTMLFormElement)) {
    return false
  }

  const input = form.querySelector('[type=search]')
  if (!(input instanceof HTMLInputElement)) {
    return false
  }

  const searchEngine = form.getAttribute('data-search') || null
  if (searchEngine !== 'google') {
    return true
  }

  return googleSearch(form, input)
}

/**
 * Updates the form action and method to perform a Google search.
 *
 * @param {HTMLFormElement} form - The form element to update.
 * @param {HTMLInputElement} input - The input element containing the search query.
 * @return {boolean} Returns true after updating the form.
 */
const googleSearch = (form, input) => {
  form.setAttribute('action', 'https://www.google.com/search')
  form.setAttribute('method', 'get')

  const inputValue = input.value
  const hostname = location?.hostname ?? 'localhost'
  const queryName = 'q'
  const queryValue = `${inputValue} inurl:${hostname}`

  input.setAttribute('name', queryName)
  input.value = queryValue

  return true
}

export default dataSearch
