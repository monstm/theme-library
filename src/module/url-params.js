/**
 * Updates elements with the attribute data-param-content, data-param-title, data-param-value, data-param-selected, or data-param-checked
 * with the value of the corresponding URL parameter.
 *
 * For example, if the URL is https://example.com/index.html?lang=en&name=John,
 * an element with the attribute data-param-content="lang" will have its innerHTML set to "en",
 * and an element with the attribute data-param-title="name" will have its title attribute set to "John"
 *
 * @return {undefined} This function does not return a value.
 */
const urlParams = () => {
  const urlSearchParams = new URLSearchParams(window.location.search)

  document.querySelectorAll('[data-param-value]').forEach((element) => {
    const paramValue = element.dataset?.paramValue
    if (paramValue && urlSearchParams.has(paramValue)) {
      element.value = urlSearchParams.get(paramValue)
    }
  })

  document.querySelectorAll('[data-param-selected]').forEach((element) => {
    const paramSelected = element.dataset?.paramSelected
    if (paramSelected && urlSearchParams.has(paramSelected)) {
      element.value = urlSearchParams.get(paramSelected)
    }
  })

  document.querySelectorAll('[data-param-checked]').forEach((element) => {
    const paramChecked = element.dataset?.paramChecked
    if (paramChecked && urlSearchParams.has(paramChecked)) {
      element.checked = urlSearchParams.get(paramChecked) === element.value
    }
  })

  document.querySelectorAll('[data-param-title]').forEach((element) => {
    const paramTitle = element.dataset?.paramTitle
    if (paramTitle && urlSearchParams.has(paramTitle)) {
      element.setAttribute('title', urlSearchParams.get(paramTitle))
    }
  })

  document.querySelectorAll('[data-param-content]').forEach((element) => {
    const paramContent = element.dataset?.paramContent
    if (paramContent && urlSearchParams.has(paramContent)) {
      element.innerHTML = urlSearchParams.get(paramContent)
    }
  })
}

export default urlParams
