/**
 * Adds UTM parameters to all links on the page with a valid href attribute.
 *
 * The UTM parameters are added in the following order of priority:
 * 1. data attributes (data-utm-source, data-utm-campaign, data-utm-medium, data-utm-content, data-utm-term)
 * 2. default values
 *
 * The UTM parameters are set as follows:
 * - utm_source: the hostname of the current page
 * - utm_campaign: the title of the current page
 * - utm_medium: 'website'
 * - utm_content: the title attribute of the link element, or the alt attribute if title is not present, or an empty string if both are not present
 * - utm_term: the value of the data-utm-term attribute, or an empty string if not present
 *
 * To skip adding UTM parameters to a link, add the class "skip-utm" to the link.
 *
 * @example
 * <a href="https://example.com" data-utm-source="example.com" data-utm-campaign="Example Campaign">Link</a>
 * -> <a href="https://example.com?utm_source=example.com&utm_campaign=Example+Campaign&utm_medium=website">Link</a>
 *
 * @example
 * <a href="https://example.com" title="Link Title" alt="Link Alt">Link</a>
 * -> <a href="https://example.com?utm_source=monstm.github.io&utm_campaign=Theme+Library&utm_medium=website&utm_content=Link+Title">Link</a>
 *
 * @return {undefined} This function does not return a value.
 */
const utmBuilder = () => {
  const defaultUtmSource = window.location.hostname || ''
  const defaultUtmCampaign = document?.title || ''
  const defaultUtmMedium = 'website'

  document.querySelectorAll('a[href]').forEach((element) => {
    const elementHref = element.getAttribute('href')
    if (
      !elementHref ||
      elementHref.startsWith('#') ||
      element.classList.contains('skip-utm')
    ) {
      return
    }

    const url = new URL(element.href)

    if (!url.searchParams.has('utm_source')) {
      const utmSource = element.dataset?.utmSource || defaultUtmSource
      url.searchParams.set('utm_source', utmSource)
    }

    if (!url.searchParams.has('utm_campaign')) {
      const utmCampaign = element.dataset?.utmCampaign || defaultUtmCampaign
      url.searchParams.set('utm_campaign', utmCampaign)
    }

    if (!url.searchParams.has('utm_medium')) {
      const utmMedium = element.dataset?.utmMedium || defaultUtmMedium
      url.searchParams.set('utm_medium', utmMedium)
    }

    if (!url.searchParams.has('utm_content')) {
      const elementTitle = element.getAttribute('title')
      const elementAlt = element.getAttribute('alt')
      const utmContent =
        element.dataset?.utmContent || elementAlt || elementTitle || ''

      if (utmContent) {
        url.searchParams.set('utm_content', utmContent)
      }
    }

    if (!url.searchParams.has('utm_term') && element.dataset?.utmTerm) {
      url.searchParams.set('utm_term', element.dataset.utmTerm)
    }

    element.href = url.href
  })
}

export default utmBuilder
