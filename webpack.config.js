const CopyPlugin = require('copy-webpack-plugin')
const fs = require('fs')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

const isProduction = !!process.env.CI_COMMIT_TAG
const sourceDirectory = path.resolve(__dirname, 'src')
const distributionDirectory = path.resolve(__dirname, 'dist')

const getHtmlPlugins = (sourceDirectory) => {
  const htmlPlugins = []
  const twigDirectory = path.resolve(sourceDirectory, 'twig')

  const files = fs.readdirSync(twigDirectory)
  const twigFiles = files.filter(file => path.extname(file).toLowerCase() === '.twig')

  const templateParameters = {
    isProduction,
    version: process?.env?.CI_COMMIT_TAG ?? null
  }

  twigFiles.forEach(twigFile => {
    const twigName = path.parse(twigFile).name
    const template = path.resolve(twigDirectory, twigFile)
    const filename = `html/${twigName}.html`

    htmlPlugins.push(new HtmlWebpackPlugin({
      template,
      filename,
      templateParameters
    }))
  })

  return htmlPlugins
}

let mode = 'production'
let outputFilename = '[name].js'

if (!isProduction) {
  mode = 'development'
  outputFilename = '[name]-[contenthash].js'
}

const plugins = [
  new CopyPlugin({
    patterns: [
      { from: 'src/module', to: 'module' }
    ]
  })
].concat(getHtmlPlugins(sourceDirectory))

module.exports = {
  mode,
  entry: {
    bundle: './src/bundle.js',
    adhelper: './src/adblock-honeypot.js'
  },
  output: {
    path: distributionDirectory,
    filename: `./js/${outputFilename}`,
    clean: true
  },
  module: {
    rules: [
      {
        test: /\.twig$/,
        use: ['twig-loader']
      }
    ]
  },
  plugins,
  devServer: {
    static: {
      directory: distributionDirectory
    },
    compress: true,
    port: 9000
  },
  resolve: {
    fallback: {
      fs: false
    }
  },
  devtool: false
}
